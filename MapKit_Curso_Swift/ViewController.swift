//
//  ViewController.swift
//  TestLocation
//
//  Created by Abraham Vazquez on 5/22/19.
//  Copyright © 2019 Abraham Vazquez. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class ViewController: UIViewController
{
    
    @IBOutlet weak var map: MKMapView!
    
    let locationManager = CLLocationManager()
    //18.6581421,-88.4078573,17
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        let latitud: CLLocationDegrees = 18.6581421
        let longitud: CLLocationDegrees = -88.4078573
        //
        //        let latitudDelta: CLLocationDegrees = 0.5
        //        let longitudDelta: CLLocationDegrees = 0.5
        //
        //        let span = MKCoordinateSpan(latitudeDelta: latitudDelta, longitudeDelta: longitudDelta)
        //
        //        let coordinates = CLLocationCoordinate2D(latitude: latitud, longitude: longitud)
        //
        //        let region = MKCoordinateRegion(center: coordinates, span: span)
        //
        //        self.map.setRegion(region, animated: true)
    }
}

extension ViewController: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first{
            print("Aqui se encuentra el usuario")
            print("Latitud:  \(location.coordinate.latitude)")
            print("Longitud: \(location.coordinate.longitude)")
            let latitudDelta: CLLocationDegrees = 0.5
            let longitudDelta: CLLocationDegrees = 0.5
            let span = MKCoordinateSpan(latitudeDelta: latitudDelta, longitudeDelta: longitudDelta)
            let coordinates = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: coordinates, span: span)
            self.map.setRegion(region, animated: true)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .authorizedAlways:
            print("El usuario ha autorizado por siempre")
        case .authorizedWhenInUse:
            print("")
        case .denied:
            print("")
        case .restricted:
            print("")
        default:
            break
        }
        
    }
}

